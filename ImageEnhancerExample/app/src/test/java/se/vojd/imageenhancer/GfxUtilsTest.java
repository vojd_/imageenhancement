package se.vojd.imageenhancer;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by vojd on 8/20/2017.
 */

public class GfxUtilsTest {

    @Test
    public void generateHistogram_hsv_isCorrect() throws Exception {
        float[] hsv = new float[] {0f, 0f, 1f};
        float[][] hsvPixels = new float[5][3];
        for (int i=0; i<hsvPixels.length; i++) {
            hsvPixels[i] = hsv;
        }

        int[] expected = new int[]{0, 5, 0, 0, 0};

        int[] histogram = GfxUtils.generateHistogram(hsvPixels, 5);
        assertArrayEquals(expected, histogram);
    }

    @Test
    public void generateHistogram_v_isCorrect() throws Exception {
        int[] V = new int[] { 1,1,1,1,2,2,2,2};
        int[] expected = new int[] {0, 4, 4, 0};

        int[] result = GfxUtils.generateHistogram(V, 4);
        assertEquals(4, result.length);
        assertArrayEquals(expected, result);
    }

    @Test
    public void generateHistogram_returnsHistogramOfNBins() throws Exception {
        int[] V = new int[10];
        int[] result = GfxUtils.generateHistogram(V, 256);
        assertEquals(256, result.length);
    }

    @Test
    public void HSVToVint_isCorrect() throws Exception {
        float[][] hsvPixels = new float[3][3];
        for (int i=0; i<hsvPixels.length; i++) {
            hsvPixels[i][0] = 0.5f;
            hsvPixels[i][1] = 0.5f;
            hsvPixels[i][2] = 0.5f;
        }
        int[] expected = {127, 127, 127};
        int[] result = GfxUtils.HSVToVint(hsvPixels);
        assertArrayEquals(expected, result);
    }

    @Test
    public void VintToHSV_isCorrect() throws Exception {
        int[] V = new int[] {255, 128, 1};
        float[][] hsvPixels = new float[3][3];
        for (int i=0; i<hsvPixels.length; i++) {
            hsvPixels[i][0] = 0.5f;
            hsvPixels[i][1] = 0.5f;
            hsvPixels[i][2] = 0.5f;
        }
        float[][] result = GfxUtils.VintToHSV(V, hsvPixels);

        assertEquals(1.f,     result[0][2], 0.02);
        assertEquals(.5f,     result[1][2], 0.02);
        assertEquals(0.0039f, result[2][2], 0.02);
    }
}
