package se.vojd.imageenhancer;


import org.junit.Test;
import static org.junit.Assert.*;

public class MathUtilsTest {

    @Test
    public void sum_isCorrect() throws Exception {
        // Arrange
        int[] v = {10, 10, 10, 10};

        // Assert
        int s = MathUtils.sum(v, 0, v.length-1);

        // Assert
        assertEquals(40, s, 0.0f);
    }

    @Test
    public void cumsum_isCorrect() throws Exception {
        // Arrange
        int[] v = {1, 2, 3};
        int[] expected = {1, 3, 6};
        // Act
        int[] s = MathUtils.cumsum(v);

        // Assert
        assertArrayEquals(expected, v);

    }

    @Test
    public void divideElements_isCorrect() throws Exception {
        int[] v = {10, 10, 10};
        int[] expected = {5, 5, 5};

        int s[] = MathUtils.normalize(v, 2);

        assertArrayEquals(expected, s);
    }

    @Test
    public void min() throws Exception {
        int[] v = {1,2,3,4,5};
        int expected = 1;
        assertEquals(expected, MathUtils.min(v));
    }

    @Test
    public void max() throws Exception {
        int[] v = {1,2,3,4,5};
        int expected = 5;
        assertEquals(expected, MathUtils.max(v));
    }
}
