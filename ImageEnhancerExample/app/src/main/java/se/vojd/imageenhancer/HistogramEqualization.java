package se.vojd.imageenhancer;

import android.graphics.Bitmap;

import com.softa.imageenhancer.ImageEnhancer;

public class HistogramEqualization implements ImageEnhancer {
    private int progress = 0;

    @Override
    public Bitmap enhanceImage(Bitmap bitmap, int configuration) {
        return histogramEqualization(bitmap);
    }

    @Override
    public int getProgress() {
        return progress;
    }

    @Override
    public String[] getConfigurationOptions() {
        return new String[] { "Histogram equalization" };
    }

    private Bitmap histogramEqualization(Bitmap image) {
        progress = 5;
        int[] pixels = GfxUtils.getPixels(image);
        float[][] hsvPixels = GfxUtils.toHSV(pixels);

        // The V values that we want to transform
        int[] V = GfxUtils.HSVToVint(hsvPixels);
        int[] histogram = GfxUtils.generateHistogram(V, 256);
        progress = 10;

        int[] cdf = MathUtils.cumsum(histogram);
        Utils.logArray(cdf, "cdf");

        cdf = MathUtils.normalize(cdf, image.getWidth() * image.getHeight());
        progress = 50;
        Utils.logArray(cdf, "cdf");

        for(int i=0; i<hsvPixels.length; i++) {
             V[i] = cdf[V[i]];
        }

        GfxUtils.VintToHSV(V, hsvPixels);
        progress = 90;
        return GfxUtils.createBitmap(image, GfxUtils.toRGB(hsvPixels));
    }
}
