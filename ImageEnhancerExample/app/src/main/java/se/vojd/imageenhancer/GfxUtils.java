package se.vojd.imageenhancer;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by vojd on 8/20/2017.
 */

public class GfxUtils {

    static int[] getPixels(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int [] pixels = new int[width * height];
        image.getPixels(pixels, 0, width, 0, 0, width, height);
        return pixels;
    }

    static float[][] toHSV(int[] pixels) {
        float [][] hsvPixels = new float[pixels.length][3];
        for (int i=0; i<pixels.length; i++) {

            Color.RGBToHSV(
                    Color.red(pixels[i]),
                    Color.green(pixels[i]),
                    Color.blue(pixels[i]),
                    hsvPixels[i]);
        }
        return hsvPixels;
    }

    static int[] toRGB(float[][] hsvPixels) {
        int[] out = new int[hsvPixels.length];
        for (int i=0; i<hsvPixels.length; i++) {
            out[i] = Color.HSVToColor(hsvPixels[i]);
        }
        return out;
    }

    static Bitmap createBitmap(Bitmap bitmap, int[] pixels) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap modBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        modBitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return modBitmap;
    }

    /**
     * Generate histogram from the V channel in an HSV image
     * @param hsvPixels float[]
     * @param nofBins Number of grayscales in image
     * @return
     */
    static int[] generateHistogram(float[][] hsvPixels, int nofBins) {

        int[] histogram = new int[nofBins];

        for(int j=0; j<nofBins; j++) {
            int n = 0;
            for(int i=0; i<hsvPixels.length; i++) {
                if(hsvPixels[i][2] == j) {
                    n++;
                }
            }
            histogram[j] = n;
        }
        return histogram;
    }

    static int[] generateHistogram(int[] V, int nofBins) {
        int[] histogram = new int[nofBins];
        for(int j=0; j<nofBins; j++) {
            int n = 0;
            for(int i=0; i<V.length; i++) {
                if(V[i] == j) {
                    n++;
                }
            }
            histogram[j] = n;
        }
        return histogram;
    }

    static int[] HSVToVint(float[][] hsvPixels) {
        int[] v = new int[hsvPixels.length];
        for (int i=0; i<hsvPixels.length; i++) {
            v[i] = (int)(hsvPixels[i][2] * 255);
        }
        return v;
    }

    static float[][] VintToHSV(int[] V, float[][] hsvPixels) {

        for (int i=0; i<hsvPixels.length; i++) {
            hsvPixels[i][2] = V[i] / 255.0f;
        }
        return hsvPixels;
    }
}
