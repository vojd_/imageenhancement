package se.vojd.imageenhancer;

import android.util.Log;

/**
 * Created by vojd on 8/22/2017.
 */

public class Utils {

    public static void logArray(int[] v, String name) {
        Log.d("INFO", "logArray " + name);
        for (int i=0; i<v.length; i++)
            Log.d("INFO", " : "  + v[i]);
    }

    public static void logArray(float[][] v, String name) {
        Log.d("INFO", "logArray " + name);
        for (int i=0; i<v.length; i++)
            Log.d("INFO", " : "
                    + v[i][0] + " "
                    + v[i][1] + " "
                    + v[i][02] + " "
            );
    }
}
