package se.vojd.imageenhancer;

import org.jetbrains.annotations.Contract;

/**
 * Created by vojd on 8/20/2017.
 */

class MathUtils {
    @org.jetbrains.annotations.Contract(pure = true)
    static int sum(int[] v, int start, int end) {
        int s = 0;
        for(int i=start; i<=end; i++) {
            s += v[i];
        }
        return s;
    }

    static int sum(int[] v) {
        int s = 0;
        for (int aV : v) {
            s += aV;
        }
        return s;
    }

    @Contract(pure = true)
    static int[] cumsum(int[] v) {
        int[] cs = new int[v.length];

        for(int i=0; i<v.length; i++) {
            cs[i] = sum(v, 0, i);
        }
        return cs;
    }

    @Contract(pure = true)
    static int[] normalize(int[] v, int d) {
        int s[] = new int[v.length];
        for (int i=0; i<v.length; i++) {
            s[i] = v[i] * 255 / d;
        }
        return s;
    }

    static int min(int[] v) {
        int p =  v[0];
        for (int i=1; i<v.length; i++) {
            p = Math.min(p, v[i]);
        }
        return p;
    }

    static int max(int[] v) {
        int p =  v[0];
        for (int i=1; i<v.length; i++) {
            p = Math.max(p, v[i]);
        }
        return p;
    }
}
